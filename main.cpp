#include "ObjectTracker.h"

#include <OpenNI.h>

#include <string>
#include <fstream>
#include <iostream>

int main(int argc, char** argv)
{
    const std::string file_name = "calibration.txt";
    std::ofstream file_stream;
    file_stream.open(file_name);

    openni::Status rc = openni::STATUS_OK;
    rc = openni::OpenNI::initialize();
    if (rc != openni::STATUS_OK) {
        printf("%s: Initialize failed\n%s\n", argv[0], openni::OpenNI::getExtendedError());
        return 1;
    }

    openni::Array<openni::DeviceInfo> deviceList;
    openni::OpenNI::enumerateDevices(&deviceList);

    std::vector<ObjectTracker*> trackers;

    for (int i=0; i<deviceList.getSize(); ++i){
        openni::DeviceInfo info = deviceList[i];
        std::cout << "========================================\n" ;
        std::cout << "Name: "       << std::string(info.getName()) << '\n';
        std::cout << "URI: "        << std::string(info.getUri()) << '\n';
        std::cout << "Vendor: "     << std::string(info.getVendor()) << '\n';
        std::cout << "VendorID: "   << info.getUsbVendorId() << '\n';
        std::cout << "ProductID: "  << info.getUsbProductId() << '\n';

        ObjectTracker* tracker = new ObjectTracker();
        trackers.push_back( tracker );
        tracker->initialize( info.getUri() );

    }
    std::cout << "========================================\n";

    while(true)
    {
        for (ObjectTracker* tracker : trackers){
            tracker->update();

            file_stream << tracker->timeOfLastUpdate() << '\t'
                        << tracker->position_world_.transpose() << '\t'
                        << tracker->stddev_depth << '\t';
        }

        file_stream << '\n';

        char key = cv::waitKey(1);
        if(key==27) break;          // Escape key number
    }


    for (ObjectTracker* tracker : trackers){
        delete tracker;
    }

    file_stream.close();

}
