#include "ObjectTracker.h"

ObjectTracker::ObjectTracker()
{
    transform_world_.setIdentity();
    std::cout << "rotation world:\n" << transform_world_.rotation() << std::endl;
    std::cout << "position world:\n" << transform_world_.translation().transpose() << std::endl;
}

ObjectTracker::~ObjectTracker()
{
    elapsed_seconds = end-start;
    std::cout << "loop counter: " << loop_counter << '\n'
              << "elapsed time: " << elapsed_seconds.count() << "s\n"
              << "update freq : " << loop_counter/elapsed_seconds.count() << "Hz\n";

    if (TRACK_OBJECT && PRINT_OBJECT_POS){
        object_pos_stream.close();
    }

    if (TRACK_OBJECT && PRINT_OBJECT_DEPTH){
        object_depth_stream.close();
    }

    closeOpenNI();
    closeOpenCV();
}

/** connect to primesense sensor */
void ObjectTracker::initialize(const std::string& device_URI)
{
    initializeDevice(device_URI);
    initializeOpenNI();
    initializeOpenCV();
    initializeTracking();
    initializeGraphics();

    if (TRACK_OBJECT && PRINT_OBJECT_POS){
        object_pos_file +='-'+device_URI+".txt";
        std::replace(object_pos_file.begin(),object_pos_file.end(),'/','-');
        object_pos_stream.open(object_pos_file);
    }

    if (TRACK_OBJECT && PRINT_OBJECT_DEPTH){
        object_depth_file += device_URI+".txt";
        std::replace(object_depth_file.begin(),object_depth_file.end(),'/','-');
        object_depth_stream.open(object_depth_file);
    }

    start = std::chrono::system_clock::now();
}

/** update */
void ObjectTracker::update()
{
    updateOpenNI();

    end = std::chrono::system_clock::now();
    elapsed_seconds = end-start;
    if (TRACK_OBJECT && PRINT_OBJECT_POS){
        object_pos_stream << elapsed_seconds.count() << '\t';
    }
    if (TRACK_OBJECT && PRINT_OBJECT_DEPTH){
        //object_depth_stream << elapsed_seconds.count() << '\t';
    }

    updateOpenCV();
    updateTracking();

    updateGraphics();

    ++loop_counter;
}

/** initialize device */
void ObjectTracker::initializeDevice(const std::string& device_URI)
{
    openni::Status rc = openni::STATUS_OK;

    rc = openni::OpenNI::initialize();
    if (rc != openni::STATUS_OK){ printOpenNIError("Initialization failed"); }

    if (device_URI.empty()){
        rc = device.open(openni::ANY_DEVICE);
    } else {
        rc = device.open(device_URI.c_str());

        //rename all windows with unique device id
        device_uri = device_URI;
        window_depth    += "-"+device_URI;
        window_rgb      += "-"+device_URI;
        window_ir       += "-"+device_URI;
        window_filter   += "-"+device_URI;
        window_object   += "-"+device_URI;
    }
    if (rc != openni::STATUS_OK){ printOpenNIError("Device open failed"); }
}

/** initialize openni */
void ObjectTracker::initializeOpenNI()
{
    openni::Status rc = openni::STATUS_OK;

    mode.setResolution( 320, 240 );
    mode.setFps( 60 );

    if (USE_DEPTH)
    {
        rc = depthStream.create(device, openni::SENSOR_DEPTH);
        if (rc != openni::STATUS_OK){ printOpenNIError("Couldn't start depth stream"); }

        rc = depthStream.start();
        if (rc != openni::STATUS_OK){ printOpenNIError("Couldn't start depth stream"); }

        streams[0] = &depthStream;

        mode.setPixelFormat( openni::PIXEL_FORMAT_DEPTH_100_UM );
        //mode.setPixelFormat( PIXEL_FORMAT_DEPTH_1_MM );
        rc = depthStream.setVideoMode( mode );
        if (rc != openni::STATUS_OK){ printOpenNIError("Couldn't set depth params\n%s\n"); }
    }

    if (USE_COLOR)
    {
        rc = colorStream.create(device, openni::SENSOR_COLOR);
        if (rc != openni::STATUS_OK){ printOpenNIError("Couldn't start color stream"); }

        rc = colorStream.start();
        if (rc != openni::STATUS_OK){ printOpenNIError("Couldn't start color stream"); }

        streams[1] = &colorStream;

        mode.setPixelFormat( openni::PIXEL_FORMAT_RGB888 );
        rc = colorStream.setVideoMode( mode );
        if (rc != openni::STATUS_OK){ printOpenNIError("Couldn't set color params\n%s\n"); }
    }

    if (USE_IR)
    {
        rc = irStream.create(device, openni::SENSOR_IR);
        if (rc != openni::STATUS_OK){ printOpenNIError("Couldn't start ir stream"); }

        rc = irStream.start();
        if (rc != openni::STATUS_OK){ printOpenNIError("Couldn't start ir stream"); }

        streams[2] = &irStream;

        mode.setPixelFormat( openni::PIXEL_FORMAT_GRAY16 );
        rc = irStream.setVideoMode( mode );
        if (rc != openni::STATUS_OK){ printOpenNIError("Couldn't set ir params\n%s\n"); }
    }

    // If the depth/color synchronisation is not necessary, run time is faster :
    //device.setDepthColorSyncEnabled( false );

    // Otherwise, the streams can be synchronized with a reception in the order of our choice :
    device.setDepthColorSyncEnabled( depth_color_sync_enabled );
    if (depth_color_registration_enabled){
        device.setImageRegistrationMode( openni::IMAGE_REGISTRATION_DEPTH_TO_COLOR );
    } else {
        device.setImageRegistrationMode( openni::IMAGE_REGISTRATION_OFF);
    }
}

/** initialize opencv */
void ObjectTracker::initializeOpenCV()
{
    image_w = mode.getResolutionX();
    image_h = mode.getResolutionY();

    if (USE_DEPTH){
        depthMat = cv::Mat( cv::Size( image_w, image_h ), CV_16UC1, NULL );
    }
    if (USE_COLOR){
        colorMat  = cv::Mat( cv::Size( image_w, image_h ), CV_8UC3, NULL );
    }
    if (USE_IR){
        irMat  = cv::Mat(    cv::Size( image_w, image_h ), CV_16UC1, NULL );
    }
}

void ObjectTracker::initializeTracking()
{
    if (!TRACK_OBJECT){return;}

    hsvMat     = cv::Mat( cv::Size( image_w, image_h ), CV_8UC3, NULL );
    colorMask  = cv::Mat( cv::Size( image_w, image_h ), CV_8UC1, NULL );
    depthMask  = cv::Mat( cv::Size( image_w, image_h ), CV_8UC1, NULL );
    filterMask = cv::Mat( cv::Size( image_w, image_h ), CV_8UC1, NULL );
    objectMask = cv::Mat( cv::Size( image_w, image_h ), CV_8UC1, NULL );
}


void ObjectTracker::initializeGraphics()
{
    if (USE_DEPTH && SHOW_DEPTH){
        cv::namedWindow( window_depth, CV_WINDOW_AUTOSIZE );
    }
    if (USE_COLOR && SHOW_COLOR){
        cv::namedWindow( window_rgb,   CV_WINDOW_AUTOSIZE );
    }
    if (USE_IR && SHOW_IR){
        cv::namedWindow( window_ir,    CV_WINDOW_AUTOSIZE );
    }
    if (TRACK_OBJECT )
    {
        if (SHOW_FILTER)
        {
            cv::namedWindow( window_filter,    CV_WINDOW_AUTOSIZE );

            if (CALIBRATE_COLOR){
                cvCreateTrackbar("LowH",        window_filter.c_str(), &iLowH,  179); //Hue (0 - 179)
                cvCreateTrackbar("HighH",       window_filter.c_str(), &iHighH, 179);
                cvCreateTrackbar("LowS",        window_filter.c_str(), &iLowS,  255); //Saturation (0 - 255)
                cvCreateTrackbar("HighS",       window_filter.c_str(), &iHighS, 255);
                cvCreateTrackbar("LowV",        window_filter.c_str(), &iLowV,  255); //Value (0 - 255)
                cvCreateTrackbar("HighV",       window_filter.c_str(), &iHighV, 255);
                cvCreateTrackbar("LowDepth",    window_filter.c_str(), &iLowDepth, 65535); //Depth (0-65535)
                cvCreateTrackbar("HighDepth",   window_filter.c_str(), &iHighDepth, 65535);
            }
            if (USE_SMOOTHING){
                cvCreateTrackbar("BlurSigma",   window_filter.c_str(), &iBlurSigma, 10);
            }
        }
        if (SHOW_OBJECT)
        {
            cv::namedWindow( window_object,    CV_WINDOW_AUTOSIZE );

            switch (OBJECT_DETECTION)
            {
            case DETECT_HOUGH:
                cvCreateTrackbar("CannyThreshold", window_object.c_str(), &iCannyThreshold, 500);
                cvCreateTrackbar("AccumThreshold", window_object.c_str(), &iAccumThreshold, 500);
                break;
            case DETECT_MOMENT:
                cvCreateTrackbar("Threshold", window_object.c_str(), &iThreshold, 500);
                break;
            }
        }
    }
}


/** update openni */
void ObjectTracker::updateOpenNI()
{
    openni::Status rc = openni::STATUS_OK;

    if (USE_DEPTH){
        rc = depthStream.readFrame(&depthFrame);
    }

    if (USE_COLOR){
        rc = colorStream.readFrame(&colorFrame);
    }

    if (USE_IR){
        rc = irStream.readFrame(&irFrame);
    }
}

/** update opencv */
void ObjectTracker::updateOpenCV()
{
    if (USE_DEPTH && depthFrame.isValid()){
        depthMat.data = (uchar*) depthFrame.getData();
        offsetImage(depthMat, 0, depth_offset_x, depth_offset_y);
    }

    if (USE_COLOR && colorFrame.isValid()){
        colorMat.data = (uchar*) colorFrame.getData();
        cv::cvtColor( colorMat, colorMat, CV_BGR2RGB );
    }

    if (USE_IR && irFrame.isValid()){
        irMat.data = (uchar*) irFrame.getData();
        irMat.convertTo(irMat, CV_8U);  // OpenCV displays 8-bit data (I'm not sure why?)
    }
}

/** update tracking */
void ObjectTracker::updateTracking()
{
    if (!TRACK_OBJECT){return;}

    object_in_frame = false;

    pos_x_pix = 0;      pos_x = 0;
    pos_y_pix = 0;      pos_y = 0;
    pos_z_pix = 0;      pos_z = 0;
    stddev_z_pix = 0;   stddev_z = 0;
    radius_pix = 0;     radius = 0;

    // color filter
    cv::cvtColor(colorMat, hsvMat, CV_RGB2HSV);
    cv::inRange(hsvMat, cv::Scalar(iLowH, iLowS, iLowV),
                        cv::Scalar(iHighH, iHighS, iHighV),
                colorMask);

    // depth filter
    cv::inRange(depthMat, iLowDepth, iHighDepth, depthMask);
    cv::bitwise_and(colorMask, depthMask, filterMask);

    if (USE_SMOOTHING){
        cv::GaussianBlur(filterMask, filterMask,cv::Size(5,5),iBlurSigma);
    }

    switch (OBJECT_DETECTION)
    {
    case DETECT_HOUGH:
    {
        int min_ball_diam_pix = 3;
        int max_ball_diam_pix = 20;

        std::vector<cv::Vec3f> circles;
        cv::HoughCircles(filterMask, circles, CV_HOUGH_GRADIENT, 1, min_ball_diam_pix,
                         iCannyThreshold, iAccumThreshold,
                         min_ball_diam_pix, max_ball_diam_pix);
        //std::cout << "num circles: " << circles.size() << std::endl;
        for( size_t i = 0; i < circles.size(); i++ )
        {
            cv::Scalar color(255,0,0);
            cv::Point center(cvRound(circles[i][0]), cvRound(circles[i][1]));
            int radius = cvRound(circles[i][2]);
            //circle( colorMat, center, 3, Scalar(0,0,255), -1, cv::LINE_8, 0 ); // draw the circle center
            cv::circle( colorMat, center, radius, color, 1, cv::LINE_8, 0 ); // draw the circle outline
        }

        //use first circle for mask
        objectMask.setTo(cv::Scalar(0));
        if (circles.size()>0)
        {
            object_in_frame = true;

            pos_x_pix = circles[0][0];
            pos_y_pix = circles[0][1];

            cv::Point center(cvRound(pos_x_pix), cvRound(pos_y_pix));
            radius_pix = cvRound(circles[0][2]);
            cv::circle( objectMask, center, radius_pix, 255, -1, cv::LINE_8, 0 );
        } else {
            std::cout << "no hough circles found" << std::endl;
        }

        break;
    }
    case DETECT_MOMENT:
    {
        cv::inRange(filterMask, iThreshold, 255, objectMask);
        cv::Moments m = moments(objectMask, true);
        double area = m.m00;
        if (area>min_object_area_pix)
        {
            object_in_frame = true;
            pos_x_pix = m.m10/area;
            pos_y_pix = m.m01/area;
            radius_pix = sqrt(area/M_PI);
        }

        break;
    }
    }

    if (object_in_frame)
    {
        //expand object search space
        cv::Point center(cvRound(pos_x_pix), cvRound(pos_y_pix));
        cv::circle( objectMask, center, radius_pix + SEARCH_AREA_BUFFER, 255, -1, cv::LINE_8, 0 );

        //filter out any depth that is not in depth range, after the blur and expansion
        cv::inRange(depthMat, iLowDepth, iHighDepth, depthMask);
        cv::bitwise_and(objectMask, depthMask, objectMask);

        //grab the closest objects, to remove background
        double minVal=0, maxVal=0;
        cv::minMaxIdx(depthMat, &minVal, &maxVal, NULL, NULL, objectMask);
        std::cout << "minVal: " << minVal << std::endl;
        std::cout << "minVal: " << maxVal << std::endl;

        int search_dist_buffer;
        switch (depthStream.getVideoMode().getPixelFormat()){
            case openni::PIXEL_FORMAT_DEPTH_100_UM:
                search_dist_buffer = 10*SEARCH_DIST_BUFFER;
                break;
            case openni::PIXEL_FORMAT_DEPTH_1_MM:
                search_dist_buffer = SEARCH_DIST_BUFFER;
                break;
            default:
                search_dist_buffer = SEARCH_DIST_BUFFER;
                break;
        }

        int total = minVal;
        int count = 1;
        for (int row=0; row<image_h; ++row){
            for (int col=0; col<image_w; ++col){
                if (objectMask.at<unsigned char>(row,col)>0){
                    int depth = (int)depthMat.at<uint16_t>(row,col);
                    int dist = depth - minVal;
                    if (dist < search_dist_buffer){
                        total += depth;
                        count += 1;
                    } else {
                        objectMask.at<unsigned char>(row,col) = 0;
                    }

                    if (PRINT_OBJECT_DEPTH){
                        object_depth_stream << depth << '\n';
                    }
                }
            }
        }

        pos_z_pix = ((float)total)/count;
        stddev_z_pix = count;

        /*
        //calculate the mean depth
        cv::Scalar mean;
        cv::Scalar stddev;
        cv::meanStdDev ( depthMat, mean, stddev, objectMask );
        pos_z_pix = mean.val[0];
        stddev_z_pix = stddev.val[0];
        */


        if (PRINT_OBJECT_DEPTH){
            for (int row=0; row<image_h; ++row){
                for (int col=0; col<image_w; ++col){
                    if (objectMask.at<unsigned char>(row,col)>0){
                        object_depth_stream << (int)depthMat.at<uint16_t>(row,col) << '\n';
                    }
                }
            }
        }


        //std::cout << "(" << pos_x_pix << ", " << pos_y_pix << ", " << pos_z_pix << ") " << stddev_z_pix << std::endl;

        // convert object pos to device frame in mm
        openni::CoordinateConverter::convertDepthToWorld(depthStream,
                                                         pos_x_pix, pos_y_pix, pos_z_pix,
                                                         &pos_x, &pos_y, &pos_z);

        // convert to mm
        switch (depthStream.getVideoMode().getPixelFormat())
        {
        case openni::PIXEL_FORMAT_DEPTH_100_UM:
        {
            pos_z = pos_z/10.0; //z conversion has a bug
            stddev_z = stddev_z_pix/10.0;
            radius = radius_pix/10.0;
            break;
        }
        case openni::PIXEL_FORMAT_DEPTH_1_MM:
        {
            stddev_z = stddev_z_pix;
            radius = radius_pix;
            break;
        }
        default:
        {
            stddev_z = stddev_z_pix;
            radius = radius_pix;
            std::cout << "WARNING. THIS DEPTH FORMAT IS NOT IMPLEMENTED." << std::endl;
        }
        }
    }

    // update public value
    position_raw_(0) = pos_x;
    position_raw_(1) = pos_y;
    position_raw_(2) = pos_z;
    stddev_depth = stddev_z;

    position_world_ = transform_world_ * position_raw_;



    std::cout << "(" << pos_x << ", " << pos_y << ", " << pos_z << ") " << stddev_z << std::endl;

    if (PRINT_OBJECT_POS){
        object_pos_stream << pos_x << '\t'
                          << pos_y << '\t'
                          << pos_z << '\t'
                          << stddev_z << '\t'
                          << radius << '\n';
    }
}


/** update plots */
void ObjectTracker::updateGraphics()
{
    //plot ball position on top of rgb and depth image
    cv::Scalar color(0,255,0);
    cv::Point center(cvRound(pos_x_pix), cvRound(pos_y_pix));
    cv::circle( colorMat, center, radius_pix, color, 1, cv::LINE_8, 0 );
    cv::circle( depthMat, center, radius_pix, 65535, 1, cv::LINE_8, 0 );

    //draw a circle at the centroid
    //cv::circle( objectMask, center, 0.1*stddev_z_pix, 255, 1, cv::LINE_8, 0 );

    //draw the depth
    float offset = 12100;
    float scaling = 0.05;
    cv::Point depth_indicator(cvRound(1/10*objectMask.cols), scaling*(pos_z_pix - offset));
    cv::circle( objectMask, depth_indicator, 5, 255, -1, cv::LINE_8, 0 );


    if (USE_DEPTH && SHOW_DEPTH && depthFrame.isValid()){
        cv::imshow( window_depth, depthMat );
    }

    if (USE_COLOR && SHOW_COLOR && colorFrame.isValid()){
        cv::imshow( window_rgb, colorMat );
    }

    if (USE_IR && SHOW_IR && irFrame.isValid()){
        cv::imshow( window_ir, irMat );
    }

    if (TRACK_OBJECT){
        if (SHOW_FILTER){
            cv::imshow( window_filter, filterMask);
        }
        if (SHOW_OBJECT){
            cv::imshow( window_object, objectMask);
        }
    }
}

/** close openni */
void ObjectTracker::closeOpenNI()
{

    if (USE_DEPTH){
        depthStream.stop();
        depthStream.destroy();
    }

    if (USE_COLOR){
        colorStream.stop();
        colorStream.destroy();
    }

    if (USE_IR){
        irStream.stop();
        irStream.destroy();
    }

    device.close();
    openni::OpenNI::shutdown();
}

/** close openCV */
void ObjectTracker::closeOpenCV()
{
    if (USE_DEPTH && SHOW_DEPTH){
        cv::destroyWindow( window_depth );
    }

    if (USE_COLOR && SHOW_COLOR){
        cv::destroyWindow( window_rgb );
    }

    if (USE_IR && SHOW_IR){
        cv::destroyWindow( window_ir );
    }
    if (TRACK_OBJECT)
    {
        if (SHOW_FILTER){
            cv::destroyWindow( window_filter);
        }
        if (SHOW_OBJECT){
            cv::destroyWindow( window_object);
        }
    }
}

void ObjectTracker::offsetImage(cv::Mat &image, cv::Scalar bordercolour, int xoffset, int yoffset)
{
    //cv::Mat temp(image.rows-xoffset, image.cols-yoffset, image.type(), bordercolour);
    //cv::Mat temp2(image.rows-xoffset, image.cols-yoffset, image.type(), bordercolour);

    cv::Mat temp(image.rows, image.cols, image.type(), bordercolour);

    int new_cols = image.cols-abs(xoffset);
    int new_rows = image.rows-abs(yoffset);

    if      (xoffset>=0 && yoffset>=0){
        image(cv::Rect(xoffset,yoffset, new_cols,new_rows)).copyTo(\
         temp(cv::Rect(0,0,             new_cols,new_rows)));
    }
    else if (xoffset>=0 && yoffset<0){
        image(cv::Rect(xoffset,0,       new_cols,new_rows)).copyTo(\
         temp(cv::Rect(0,abs(yoffset),  new_cols,new_rows)));
    }
    else if (xoffset<0 && yoffset>=0){
        image(cv::Rect(0,yoffset,       new_cols,new_rows)).copyTo(\
         temp(cv::Rect(abs(xoffset),0,  new_cols,new_rows)));
    }
    else {//(xoffset<0 && yoffset<0){
        image(cv::Rect(0,0,             new_cols,new_rows)).copyTo(\
         temp(cv::Rect(abs(xoffset),abs(yoffset), new_cols,new_rows)));
    }

    image = temp.clone();
        //image.copyTo(temp);
    //cv::Mat offset(cvRect())



   // cv::Mat temp(image.rows+2*yoffset,image.cols+2*xoffset,image.type(),bordercolour);
    //temp(cvRect(xoffset,yoffset,image.cols,image.rows));
    //cv::Mat roi(temp(cvRect(xoffset,yoffset,image.cols,image.rows)));
    //image.copyTo(roi);
    //image=temp.clone();
}

/** print openni error */
void ObjectTracker::printOpenNIError(const std::string& message)
{
    printf("Error:%s\n%s\n", &message[0], openni::OpenNI::getExtendedError());
    openni::OpenNI::shutdown();
    exit(1);
}
