#ifndef OBJECTTRACKER_H
#define OBJECTTRACKER_H

#include <OpenNI.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <Eigen/Dense>

#include <string>
#include <iostream>
#include <fstream>
#include <chrono>

/**
 * @brief The ObjectTracker class
 * 1) Collects data from primesensor, such as asus xtion pro live.
 * 2) Applies a color filter and depth filter
 * 3) Smooths filtered image
 * 4) Applies thresholding or hough transform
 * 5) Finds closest depth within selected region (from step 4)
 * 6) Finds all points within some distance of closest point, within selected region
 * 7) Averages depths
 * 8) Calculates world coordinates of point
 */

class ObjectTracker
{
public:
    ObjectTracker();
    virtual ~ObjectTracker();

    /** connect to primesense sensor */
    void initialize(const std::string& device_URI = "");

    /** update */
    void update();

    /** time of last update */
    double timeOfLastUpdate(){ return elapsed_seconds.count(); }

    /** object in last update frame */
    bool objectInLastFrame(){ return object_in_frame; }

public:

    /// position of object in device frame (mm)
    Eigen::Vector3d position_raw_;

    /// position of object in world frame (mm)
    Eigen::Vector3d position_world_;

    /// transformation of device in world frame, i.e. pos_world = T * pos_raw
    Eigen::Affine3d transform_world_;

    /// standard deviation of object depth (mm)
    float stddev_depth = 0;

private:

    /** initialize device */
    void initializeDevice(const std::string& device_URI = "");

    /** initialize openni */
    void initializeOpenNI();

    /** initialize opencv */
    void initializeOpenCV();

    /** initialize object tracking */
    void initializeTracking();

    /** initialize graphics */
    void initializeGraphics();

    /** update openni */
    void updateOpenNI();

    /** update opencv */
    void updateOpenCV();

    /** update tracking */
    void updateTracking();

    /** update plots */
    void updateGraphics();

    /** close openni */
    void closeOpenNI();

    /** close openCV */
    void closeOpenCV();

    /** offset a matrix */
    static void offsetImage(cv::Mat &image, cv::Scalar bordercolour, int xoffset, int yoffset);

    /** print openni error */
    void printOpenNIError(const std::string& message);


    //==================================
    // SETTINGS
    //==================================
    const bool USE_DEPTH = true;
    const bool USE_COLOR = true;
    const bool USE_IR = false;

    const bool SHOW_DEPTH = true;
    const bool SHOW_COLOR = true;
    const bool SHOW_IR = false;
    const bool SHOW_FILTER = true;
    const bool SHOW_OBJECT = true;

    const bool TRACK_OBJECT = true;
    const bool CALIBRATE_COLOR = true;
    const bool USE_SMOOTHING = true;
    enum ObjectDetection {DETECT_HOUGH, DETECT_MOMENT};
    const ObjectDetection OBJECT_DETECTION = DETECT_MOMENT;

    /// will expand search area for object depth data by this many pixels
    const float SEARCH_AREA_BUFFER = 5;

    /// average all pixels (in search area) that are within some distance (mm) of closest pixel
    float SEARCH_DIST_BUFFER = 30;

    /// time + object position in world frames (mm) + stddev of depth (mm)
    const bool PRINT_OBJECT_POS = false;
    std::string object_pos_file = "object_pos";

    /// depth of points that make up object
    const bool PRINT_OBJECT_DEPTH = true;
    std::string object_depth_file = "raw_depth";

    /// sync the depth and color stream so that the two are captured close together
    /// slows down frame rate if used
    const bool depth_color_sync_enabled = false;

    /// uses onboard registration to tranform depth frame into color frame.
    const bool depth_color_registration_enabled = true;

    /// additional offset, for manual regisration.
    const int depth_offset_x = 0;
    const int depth_offset_y = 10;


    //==================================
    // OPENNNI
    //==================================
    std::string device_uri = "";
    openni::Device device;
    openni::VideoMode mode;
    openni::VideoStream*  streams[3];
    openni::VideoStream   depthStream, colorStream, irStream;
    openni::VideoFrameRef depthFrame,  colorFrame,  irFrame;

    //==================================
    // OPENCV
    //==================================
    /// image width and height
    int image_w, image_h;

    /// raw image
    cv::Mat depthMat, colorMat, irMat, hsvMat;

    /// object image
    cv::Mat objectMask;

    //==================================
    // OBJECT FILTERING
    //==================================
    /// filter images
    cv::Mat colorMask, depthMask, filterMask;
    int iLowH = 0;          int iHighH = 179;
    int iLowS = 0;          int iHighS = 10;
    int iLowV = 245;        int iHighV = 255;
    int iLowDepth = 400;    int iHighDepth = 30000;

    /// blurring for smoothing
    int iBlurSigma = 0;

    /// hough transform for object detection
    int iCannyThreshold = 100; //the higher threshold of the two passed to the Canny edge detector
    int iAccumThreshold = 10; //the accumulator threshold for the circle centers at the detection stage.
                               //the smaller it is, the more false circles may be detected

    /// thresholding for object detection
    int iThreshold = 150;
    int min_object_area_pix = 5;

    //==================================
    // OBJECT TRACKING
    //==================================

    bool object_in_frame = false;

    float pos_x_pix = 0;
    float pos_y_pix = 0;
    float pos_z_pix = 0;
    float stddev_z_pix = 0;
    float radius_pix = 0;

    float pos_x = 0;
    float pos_y = 0;
    float pos_z = 0;
    float stddev_z = 0;
    float radius = 0;

    //==================================
    // GRAPHICS
    //==================================
    std::string window_depth = "Depth";
    std::string window_rgb = "RGB";
    std::string window_ir = "IR";
    std::string window_filter = "Filter";
    std::string window_object = "Object";

    //==================================
    // OPERATION
    //==================================
    /// timing
    std::chrono::time_point<std::chrono::system_clock> start, end;
    std::chrono::duration<double> elapsed_seconds;
    unsigned long long loop_counter = 0;

    /// write to file
    std::ofstream object_pos_stream;
    std::ofstream object_depth_stream;








};

#endif // OBJECTTRACKER_H
