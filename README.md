**Color based object tracking using PrimeSense**
======================
Authors : Brian Soe  
Contact : bsoe at stanford.edu

# About

This project allows object tracking using PrimeSense sensors with OpenNI2 + OpenCV3. It creates a simple GUI for doing color filtering and thresholding, to select the object. It records the position of the object in world coordinates. Hough transform is also available, but disabled by default.

Compatibility:  

* asus xtion pro  
* asus xtion pro live  
* sensor io  
* kinect (not tested)  

# Installing OpenNI2

General instructions:
http://eras.readthedocs.org/en/latest/servers/body_tracker/doc/setup.html

### 1. Download and build openni2 from source, or download from website.  
Note: binary available, but would not work for me  
http://structure.io/openni  
Download and build from source:

```sh
cd ~
mkdir lib
cd lib
git clone https://github.com/occipital/openni2
cd OpenNI2
```

optionally switch do devel

```sh
git checkout develop
```

### 2. Follow README.md to install all dependencies

### 3. Edit the config file for primesense for linux usb access  
Open OpenNI2/Config/OpenNI2/Drivers/PS1080.ini  
Open OpenNI2/Config/OpenNI2/Drivers/PSLink.ini  

Change ";UsbInterface=2" to "UsbInterface=2" without the quotes in both files.

### 4. Install drivers for primesense
$ cd Packaging/Linux
$ sudo sh install.sh

### 5. Build (if building from source)  
Navigate to top level (OpenNI2)

```sh
cd ../../
make -j3
```

### 6. Test sensor

```sh
cd Bin/x64-Release
sudo ./SimpleRead
sudo ./NiViewer
```

### 7. Build library

```sh
cd ../../
cd Packaging/
python ReleaseVersion.py
cd OpenNI-Linux-x64-2.3
sudo sh install.sh
```

# Installing NITE2

### 1. Download NITE2 for body tracking, and unpack it to ~/lib/NiTE-2.2  
http://ilab.usc.edu/packages/forall/ubuntu-13.10/NiTE-Linux-x64-2.2.tar.bz2

### 2. Install

```sh
cd NiTE-2.2
sudo sh install.sh
```

### 3. Create a link to OpenNi2

```sh
cd Samples/Bin
mv OpenNI2 OpenNI2-original
ln -s ~/lib/OpenNI2/Bin/x64-Release/OpenNI2 OpenNI2
```

### 4. Run the example

```sh
sudo ./UserViewer
```

# Installing OpenCV

The following opencv build includes openni support. However, the project does not require this support.

### 1. Download OpenCV  
   http://opencv.org/

### 2. Install dependencies listed in online installation tutorial:  
   http://docs.opencv.org/doc/tutorials/introduction/linux_install/linux_install.html

### 3. Download CMake GUI

```sh
sudo apt-get install cmake-qt-gui
```

### 4. Edit the path the openni2  
   In cmake/OpenCVFindOpenNI2.cmake, add the path to oppenni2 in the find_file and find_library paths:

```cmake
elseif(UNIX OR APPLE)
  find_file(OPENNI2_INCLUDES "OpenNI.h" 
            PATHS "/usr/include/ni2" 
                  "/usr/include/openni2" 
                  $ENV{OPENNI2_INCLUDE} 
                  "/home/brian/lib/OpenNI2/Packaging/OpenNI-Linux-x64-2.3/Include" # <==========
            DOC "OpenNI2 c++ interface header")
     find_library(OPENNI2_LIBRARY "OpenNI2" 
            PATHS "/usr/lib" 
                  $ENV{OPENNI2_REDIST} 
                  "/home/brian/lib/OpenNI2/Packaging/OpenNI-Linux-x64-2.3/Redist"  # <===========
            DOC "OpenNI2 library")
endif()
```

### 5. Build

```sh
cd opencv-3.0.0
mkdir release
cmake-gui ..
```

   1. Set WITH_OPENNI2 to ON
   2. configure
   3. configure
   4. generate
   5. ext

```sh
cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local ..
make -j3
```

# Building Project 

```sh
cd ~/Documents/object-tracking
mkdir build
cd build
cmake ..
make -j3
./object-tracking
```